import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Image,
    ScrollView,
    TouchableOpacity

} from 'react-native';

class myList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lista: [
                {
                    id: "1",
                    titulo: "Chef Parrillero Grill",
                    informacion: "El Chef Parrillero ofrece una variada carta especializada en carnes a la parrilla. Nos distinguimos por la sazón, la calidad de los cortes, una surtida barra internacional y sobre todo nuestra esmerada atención. Contamos con estacionamiento privado.",
                    longitude: -71.530303,
                    latitude: -16.4135013,
                    imagen: "https://media-cdn.tripadvisor.com/media/photo-s/0f/70/85/41/darse-el-gusto-con-nuestros.jpg",
                    estado: true,
                },
                {
                    id: "2",
                    titulo: "Chifa Ka Hing",
                    informacion: "Comida china y latinoamericana, su comida es una fusión de comida china con ingredientes y sabores peruanos.",
                    longitude: -71.5308965,
                    latitude: -16.4115122,
                    imagen: "https://3.bp.blogspot.com/-cRfglK9kPvU/V5-2YW_Aj5I/AAAAAAAADcc/8VsdHsE6EV4-lHnJc8Ctbc6VTTHhefv1gCLcB/s640/chifa_ka_hing_0%255B1%255D.jpg",
                    estado: true,
                },
                {
                    id: "3",
                    titulo: "Tipika",
                    informacion: "Símbolo de la sazón Arequipeña, nace resaltando la identidad culinaraia mistiana en el Perú y el Mundo brindando lo mejor.",
                    longitude: -71.5447761,
                    latitude: -16.4062396,
                    imagen: "http://www.programasperu.com/restaurantes-peruanos/wp-content/uploads/2012/10/tipika.jpg",
                    estado: true,
                },
                {
                    id: "4",
                    titulo: "Cevicheria Arrecife",
                    informacion: "Si lo que deseas es probar un cebiche refrescante en un ambiente agradable y de frescura.",
                    longitude: -71.540469,
                    latitude: -16.408572,
                    imagen: "https://image.ibb.co/crGWRL/r-arecife.jpg",
                    estado: true,
                },
                {
                    id: "5",
                    titulo: "Sambambaias",
                    informacion: "Sambambaias es un versátil restaurante cuya amplia carta cuenta con muy variadas opciones.",
                    longitude: -71.539586,
                    latitude: -16.3917321,
                    imagen: "https://igx.4sqi.net/img/general/width960/952134_owxhLXU1cb0PhQclz4DxZyUfqnbii1j4l_K47fTuIdA.jpg",
                    estado: true,
                },
                {
                    id: "6",
                    titulo: "Zig Zag restaurant",
                    informacion: "Zig Zag Restaurant ofrece, a través de su gastronomía Alpandina, una creativa fusión culinaria que tiene hondas raíces en las montañas, donde el sol llena de equilibrio a los alimentos.",
                    longitude: -71.5376579,
                    latitude: -16.3953295,
                    imagen: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVuZSWxQUdHvIMsFra_Zk8gkKE808HQ9A5xmwO9q0-n1iwcg3Nyw",
                    estado: true,
                }
            ],
        };
    }
    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Details', {
                itemId: item.id,
                itemImage: item.imagen,
                itemTitle: item.titulo,
                itemInfo: item.informacion,
                itemLat: item.latitude,
                itemLong: item.longitude,
                itemEst: item.estado
            })}>
            <View style={styles.Contenedor}>
                <View>
                    <Image
                        source={{ uri: item.imagen }}
                        style={styles.Imagen}
                        resizeMode="cover"
                    />
                </View>
                <View style={styles.descripcion1}>
                    <Text style={styles.Nombre}>{item.titulo}</Text>
                    <Text style={styles.Descripcion}>{item.informacion}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <FlatList
                        data={this.state.lista}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        backgroundColor: '#E0F7FA',
        borderColor: 'red',
    },
    Contenedor: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 20,
        borderColor: 'red',
        justifyContent: 'flex-start',
        margin: 15,
    },
    Nombre: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    Descripcion: {
        fontSize: 12,
        color: 'gray',
    },
    descripcion1: {
        marginLeft: 20,
        marginRight: 80,
    },
    Imagen: {
        width: 100,
        height: 100,
        borderRadius: 25,
    },
});
export default myList;
